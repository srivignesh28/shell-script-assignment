#!/bin/bash -

# This script automates the deployment of php and mysql application with several options.


# Root user check
function check_root_user(){
if [ $(id -u) -ne 0 ]; then
  echo "This script must be run as a root";
  exit 1
fi
}


# Check distro , debian or rhel fedora  
function get_distro_name()
{
  . /etc/os-release
  distro_name=$ID_LIKE
  echo $distro_name;
}


# Check missing argument
if [[ $# -eq 0 ]] ; then
  echo "Missing arguments!, use --help for more info."
  exit 1
fi

# Installs apache, mysql and php
function install_lamp()
{
   #Pre checking for root user, distro
   check_root_user
   distro=$(get_distro_name)
   
   if [ $distro = "debian" ];then
     echo "----- Installing the LAMP Stack -----";
     sudo apt update
     sudo apt install apache2 mysql-server php-mysql -y
     sudo apt install php libapache2-mod-php -y 
     echo "----- LAMP Stack Instalation Completed -----";
    
   elif [ $distro = "rhel fedora" ];then
     echo "----- Installing the LAMP Stack -----";
     sudo yum update
     sudo yum install httpd mysql-server php-mysql -y
     sudo yum install php libapache2-mod-php -y 
     echo "----- LAMP Stack Instalation Completed -----";
         
   fi
}


#Checks wheather LAMP installed or not
#if not installs LAMP
function is_lamp_installed()
{
  #Pre checking for root user, distro
  check_root_user
  distro=$1
  
  if [ $distro = "debian" ];then
    is_apache=$(dpkg --get-selections | grep apache2)
    is_mysql=$(dpkg --get-selections | grep mysql)
    is_php=$(dpkg --get-selections | grep php)

    if [[ is_apache = "" || is_mysql = "" || is_php = "" ]];then
      install_lamp
    fi
  elif [ $distro = "rhel fedora" ];then
    is_httpd=$(rpm -qa | grep httpd)
    is_mysql=$(rpm -qa | grep mysql)
    is_php=$(rpm -qa | grep php)

    if [[ is_httpd = "" || is_mysql = "" || is_php = "" ]];then
      install_lamp
    fi
  fi
}


function db_config(){
   check_root_user
   sudo mysql -e "CREATE USER IF NOT EXISTS 'sri'@'localhost' IDENTIFIED BY 'sri@123';"
   sudo mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'sri'@'localhost';"
   sudo mysql -e "FLUSH PRIVILEGES;"
   
   sudo mysql -e "CREATE DATABASE IF NOT EXISTS mydb;"
   
   sudo mysql -e "exit"
}


function start_server()
{
  
   #Pre checking for root user, distro and lamp stack
   check_root_user
   distro=$(get_distro_name)
   is_lamp_installed $distro
   
   #starts the server
   
   if [ $distro = "debian" ];then
     echo "Apache2 and MySql-server is starting...";
     sudo systemctl start apache2
     sudo systemctl start mysql
     
     db_config  
     
     sudo git clone https://srivignesh28@bitbucket.org/srivignesh28/phpmyapp.git /var/www/html/myapp
     echo Site is running at http://localhost/myapp/pmysql-connect-app.php
   
   elif [ $distro = "rhel fedora" ];then
     echo "Apache2 and MySql-server is starting...";
     sudo systemctl start httpd
     sudo systemctl start mysql
     
     db_config
     
     sudo git clone https://srivignesh28@bitbucket.org/srivignesh28/phpmyapp.git /var/www/html/myapp
     echo Site is running at http://localhost/myapp/pmysql-connect-app.php 
   fi
}

function stop_server()
{
   #Pre checking for root user, distro
   check_root_user
   distro=$(get_distro_name)
   
   if [ $distro = "debian" ];then
     echo "Apache2 and MySql-server is stoping...";
     sudo systemctl stop apache2
     sudo systemctl stop mysql
     sudo rm -r /var/www/html/myapp
    
   elif [ $distro = "rhel fedora" ];then
     echo "Apache2 and MySql-server is stoping...";
     sudo systemctl stop httpd
     sudo systemctl stop mysql
     sudo rm -r /var/www/html/myapp
   fi
}

function server_status()
{
   #Pre checking for root user, distro
   check_root_user
   distro=$(get_distro_name)
   
   if [ $distro = "debian" ]; then
     echo "------ Server Status ------";
     sudo systemctl status mysql
     sudo systemctl status apache2
     
   elif [ $distro = "rhel fedora" ]; then
     echo "------ Server Status ------";
     sudo systemctl status mysql
     sudo systemctl status httpd
       
   fi
}

function validate_site()
{
  curl -I http://localhost/myapp/pmysql-connect-app.php
}


function backup_db(){

  if [ $(id -u) -eq 0 ]; then
    echo "This option needs to be run as a normal user!";
    exit 1
  fi
  
  crontab -l > cronbkp
  echo "0 0 * * * /bin/bash /home/srivignesh/backup.sh" > cronbkp
  crontab cronbkp
  rm cronbkp
  echo "Your Database backup will be done everyday midnight!"
}

function helper(){

echo "Available options or commands for this script are"
echo "--install     -> Installs the LAMP stack in the system."
echo "--start       -> Starts the apache web server and mysql server and installs LAMP stack if not installed."
echo "--stop        -> Stops the both apache web server and mysql server."
echo "--status      -> Shows the status of both web and db server."
echo "--validate    -> Validates the deployed php application site."
echo "--backup      -> Schedules a daily backup of database."
echo "--configuredb -> Configures the mysql database and user."
}

# Available options
option=$1

case $option in
  
  "--install") install_lamp;;
  "--start") start_server;;
  "--stop") stop_server;;
  "--status") server_status;;
  "--validate") validate_site;;
  "--backup") backup_db;;
  "--configuredb") db_config;;
  "--help") helper;;
  "*") echo "Option not found!";;

esac
  




