**This script automates the deployment of php and mysql application with several options.**

# USAGE

**sudo bash php-app-deployer.sh --install**

*Available options*
 <pre>
 --install     -> Installs the LAMP stack in the system.
 --start       -> Starts the apache web server and mysql server and installs LAMP stack if not installed.
 --stop        -> Stops the both apache web server and mysql server.
 --status      -> Shows the status of both web and db server.
 --validate    -> Validates the deployed php application site.
 --backup      -> Schedules a daily backup of database.
 --configuredb -> Configures the mysql database and user.
 --help        -> Provides the list of available options.`
 </pre>
 